package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net"
	"net/http"
	"os"
	"sync"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
)

var repository = &Repository{
	map[int64]Todo{},
	sync.RWMutex{},
}

// The Provider verification
func TestPactProvider(t *testing.T) {
	go startInstrumentedProvider()

	pact := createPact()

	// Verify the Provider - Tag-based Published Pacts sfor any known consumers
	// Verify the Provider with local Pact Files

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            fmt.Sprintf("http://localhost:%d", port),
		BrokerURL:                  "https://karayel.pact.dius.com.au",
		BrokerToken:                "rmfU9J6Ywu81jboafLJvHw",
		PublishVerificationResults: true,
		ProviderVersion:            "1.0.1",
		StateHandlers:              stateHandlers,
	})

	if err != nil {
		t.Log("Pact test failed")
		t.Log(err)
	}
}

// Provider state handlers
var stateHandlers = types.StateHandlers{
	"todo list": func() error {
		repository.store = todoList
		return nil
	},
	"unmarked todo": func() error {
		repository.store = singleElementTodoList
		return nil
	},
	"marked todo": func() error {
		repository.store = singleElementTodoList
		return nil
	},
	"deletable todo": func() error {
		repository.store = singleElementTodoList
		return nil
	},
	"": func() error {
		repository.store = emptyTodoList
		return nil
	},
}

// Starts the provider API with hooks for provider states.
// This essentially mirrors the main.go file, with extra routes added.
func startInstrumentedProvider() {

	handler := &Handler{
		repository,
	}

	r := mux.NewRouter()
	r.HandleFunc("/todos", handler.todos)
	r.HandleFunc("/todo/{id}", handler.todo)

	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	log.Printf("API starting: port %d (%s)", port, ln.Addr())
	log.Printf("API terminating: %v", http.Serve(ln, r))
}

// Provider States data sets
var emptyTodoList = map[int64]Todo{}

var singleElementTodoList = map[int64]Todo{
	1: {
		Id:       1,
		Text:     "test",
	}}

var todoList = map[int64]Todo{
	1: {
		Id:       1,
		Text:     "test",
		IsMarked: false,
	},
	2: {
		Id:       2,
		Text:     "test",
		IsMarked: false,
	}}

// Configuration / Test Data
var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = utils.GetFreePort()

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Consumer:                 "TodoWeb",
		Provider:                 "TodoApi",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "INFO",
	}
}
