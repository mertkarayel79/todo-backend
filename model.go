package main

type Todo struct {
	Id       int64  `json:"id"`
	Text     string `json:"text"`
	IsMarked bool   `json:"isMarked"`
}

type TodoRequest struct {
	Text string `json:"text"`
}

type TodoMarkRequest struct {
	IsMarked bool `json:"isMarked"`
}

// MIME types
const (
	MIMEApplicationJSONCharsetUTF8 = "application/json; charset=utf-8"
)

// HTTP Header Fields
const (
	HeaderContentType = "Content-Type"
)
