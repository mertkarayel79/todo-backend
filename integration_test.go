package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func Test_Integration_CreateTodoWithInvalidText(t *testing.T) {
	handler := NewHandler()

	text := ""

	response := httptest.NewRecorder()
	handler.todos(response, addTodoRequest(text))

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func Test_Integration_CreateTodoWithValidText(t *testing.T) {
	handler := NewHandler()

	text := "test"

	response := httptest.NewRecorder()
	handler.todos(response, addTodoRequest(text))

	assert.Equal(t, response.Code, http.StatusCreated)
}

func Test_Integration_GetTodosWhenListIsEmpty(t *testing.T) {
	handler := NewHandler()

	response := httptest.NewRecorder()
	handler.todos(response, getTodoRequest())

	var todoListResponseBody []Todo

	_ = json.NewDecoder(response.Body).Decode(&todoListResponseBody)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, MIMEApplicationJSONCharsetUTF8, response.Header().Get(HeaderContentType))
	assert.Equal(t, 0, len(todoListResponseBody))
}

func Test_Integration_GetTodosWhenListIsFull(t *testing.T) {
	handler := NewHandler()

	text := "test"

	createResponse := httptest.NewRecorder()
	handler.todos(createResponse, addTodoRequest(text))

	response := httptest.NewRecorder()
	handler.todos(response, getTodoRequest())

	var todoListResponseBody []Todo

	_ = json.NewDecoder(response.Body).Decode(&todoListResponseBody)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, MIMEApplicationJSONCharsetUTF8, response.Header().Get(HeaderContentType))
	assert.Equal(t, 1, len(todoListResponseBody))
}

func Test_Integration_MarkUnknownTodoId(t *testing.T) {
	handler := NewHandler()

	var id int64 = 1
	response := httptest.NewRecorder()
	handler.todo(response, revertMarkedTodoRequest(id,true))

	assert.Equal(t, http.StatusNotFound, response.Code)
}

func Test_Integration_MarkKnownTodoId(t *testing.T) {
	handler := NewHandler()

	text := "test"

	createResponse := httptest.NewRecorder()
	handler.todos(createResponse, addTodoRequest(text))

	var todoResponseBody Todo

	_ = json.NewDecoder(createResponse.Body).Decode(&todoResponseBody)

	response := httptest.NewRecorder()
	handler.todo(response, revertMarkedTodoRequest(todoResponseBody.Id, true))

	var markedResponseBody Todo

	_ = json.NewDecoder(response.Body).Decode(&markedResponseBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func Test_Integration_UnMarkUnknownTodoId(t *testing.T) {
	handler := NewHandler()

	var id int64 = 1
	response := httptest.NewRecorder()
	handler.todo(response, revertMarkedTodoRequest(id, false))

	assert.Equal(t, response.Code, http.StatusNotFound)
}

func Test_Integration_UnMarkKnownTodoId(t *testing.T) {
	handler := NewHandler()

	text := "test"

	createResponse := httptest.NewRecorder()
	handler.todos(createResponse, addTodoRequest(text))

	var todoResponseBody Todo

	_ = json.NewDecoder(createResponse.Body).Decode(&todoResponseBody)

	response := httptest.NewRecorder()
	handler.todo(response, revertMarkedTodoRequest(todoResponseBody.Id, false))

	var markedResponseBody Todo

	_ = json.NewDecoder(response.Body).Decode(&markedResponseBody)

	assert.Equal(t, http.StatusOK, response.Code)
}

func Test_Integration_DeleteUnKnownTodo(t *testing.T) {
	handler := NewHandler()

	var id int64 = 1

	response := httptest.NewRecorder()
	handler.todo(response, deleteTodoRequest(id))

	assert.Equal(t, response.Code, http.StatusNotFound)
}

func Test_Integration_DeleteKnownTodo(t *testing.T) {
	handler := NewHandler()

	text := "test"

	createResponse := httptest.NewRecorder()
	handler.todos(createResponse, addTodoRequest(text))

	var todoResponseBody Todo

	_ = json.NewDecoder(createResponse.Body).Decode(&todoResponseBody)

	response := httptest.NewRecorder()
	handler.todo(response, deleteTodoRequest(todoResponseBody.Id))

	assert.Equal(t, http.StatusNoContent, response.Code)
}

func getTodoRequest() *http.Request {
	req, _ := http.NewRequest(http.MethodGet, "/todos", nil)
	return req
}

func addTodoRequest(text string) *http.Request {
	message := map[string]interface{}{
		"text": text,
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	req, _ := http.NewRequest(http.MethodPost, "/todos", bytes.NewBuffer(bytesRepresentation))
	return req
}

func revertMarkedTodoRequest(id int64, isMarked bool) *http.Request {
	message := map[string]interface{}{
		"isMarked": isMarked,
	}

	bytesRepresentation, _ := json.Marshal(message)
	fmt.Println(fmt.Sprintf("%s%d", "/todo/", id))

	req, _ := http.NewRequest(http.MethodPatch, fmt.Sprintf("%s%d", "/todo/", id), bytes.NewBuffer(bytesRepresentation))
	req = mux.SetURLVars(req, map[string]string{
		"id": strconv.FormatInt(id, 10),
	})
	return req
}

func deleteTodoRequest(id int64) *http.Request {
	req, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s%d", "/todo/", id), nil)
	req = mux.SetURLVars(req, map[string]string{
		"id": strconv.FormatInt(id, 10),
	})
	return req
}