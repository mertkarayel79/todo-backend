package main

import (
	"sort"
	"sync"
	"time"
)

type Repository struct {
	store map[int64]Todo
	lock  sync.RWMutex
}

type IRepository interface {
	get() []Todo
	add(todo Todo) Todo
	revertMark(id int64, mark bool) bool
	delete(id int64) bool
}

func NewRepository() IRepository {
	return &Repository{
		map[int64]Todo{},
		sync.RWMutex{},
	}
}

func (i *Repository) get() []Todo {
	store := i.store
	v := make([]Todo, 0, len(store))

	keys := make([]int64, 0, len(store))
	for k := range i.store {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, j int) bool { return keys[i] < keys[j] })

	for _, k := range keys {
		v = append(v, i.store[k])
	}

	return v
}

func (i *Repository) add(todo Todo) Todo {
	i.lock.Lock()
	defer i.lock.Unlock()

	todo.Id = time.Now().UnixNano() / (1 << 22)
	todo.IsMarked = false

	i.store[todo.Id] = todo

	return todo
}

func (i *Repository) revertMark(id int64, mark bool) bool {
	value, found := i.store[id]

	if !found {
		return false
	}

	value.IsMarked = mark
	i.store[id] = value

	return true
}

func (i *Repository) delete(id int64) bool {
	_, found := i.store[id]
	if found {
		delete(i.store, id)
		return true
	} else {
		return false
	}
}