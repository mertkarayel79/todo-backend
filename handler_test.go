package main

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

var id int64 = 1
var emptyText = ""
var validText = "test"

func Test_Controller_CreateTodoWithInvalidText(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := NewMockIRepository(ctrl)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todos)

	todosHandler.ServeHTTP(response, addTodoRequest(emptyText))

	assert.Equal(t, response.Code, http.StatusBadRequest)
}

func Test_Controller_CreateTodoWithValidText(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockedTodo := Todo{
		Id:   id,
		Text: validText,
		IsMarked: false,
	}

	m := NewMockIRepository(ctrl)
	m.EXPECT().add(Todo{Text: validText}).Return(mockedTodo)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todos)

	todosHandler.ServeHTTP(response, addTodoRequest(validText))

	assert.Equal(t, response.Code, http.StatusCreated)
}

func Test_Controller_GetTodosWhenListIsEmpty(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := NewMockIRepository(ctrl)
	m.EXPECT().get().Return([]Todo{})

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todos)

	todosHandler.ServeHTTP(response, getTodoRequest())

	assert.Equal(t, response.Code, http.StatusOK)

	var todoList []Todo
	_ = json.NewDecoder(response.Body).Decode(&todoList)

	assert.Equal(t, len(todoList), 0)
}

func Test_Controller_GetTodosWhenListIsFull(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	var todos []Todo
	todos = append(todos, Todo{
		Id: 1,
		Text: "test",
		IsMarked: false,
	})
	todos = append(todos, Todo{
		Id: 2,
		Text: "test2",
		IsMarked: false,
	})

	m := NewMockIRepository(ctrl)
	m.EXPECT().get().Return(todos)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todos)

	todosHandler.ServeHTTP(response, getTodoRequest())

	assert.Equal(t, response.Code, http.StatusOK)

	var todoList []Todo
	_ = json.NewDecoder(response.Body).Decode(&todoList)

	assert.Equal(t, len(todoList), 2)
}

func Test_Controller_RevertMarkedTodoWhenTodoIsNotExist(t *testing.T){
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	request := TodoMarkRequest{IsMarked: false}

	m := NewMockIRepository(ctrl)
	m.EXPECT().revertMark(id, request.IsMarked).Return(false)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todo)

	todosHandler.ServeHTTP(response, revertMarkedTodoRequest(id, request.IsMarked))

	assert.Equal(t, response.Code, http.StatusNotFound)
}

func Test_Controller_RevertMarkedTodoWhenTodoIsExist(t *testing.T){
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	request := TodoMarkRequest{IsMarked: false}

	m := NewMockIRepository(ctrl)
	m.EXPECT().revertMark(id, request.IsMarked).Return(true)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todo)

	todosHandler.ServeHTTP(response, revertMarkedTodoRequest(id, request.IsMarked))

	assert.Equal(t, response.Code, http.StatusOK)
}

func Test_Controller_DeleteTodoWithUnKnownId(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := NewMockIRepository(ctrl)
	m.EXPECT().delete(id).Return(false)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todo)

	todosHandler.ServeHTTP(response, deleteTodoRequest(id))

	assert.Equal(t, response.Code, http.StatusNotFound)
}

func Test_Controller_DeleteTodoWithKnownId(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := NewMockIRepository(ctrl)
	m.EXPECT().delete(id).Return(true)

	handler := Handler{repository: m}

	response := httptest.NewRecorder()
	todosHandler := http.HandlerFunc(handler.todo)

	todosHandler.ServeHTTP(response, deleteTodoRequest(id))

	assert.Equal(t, response.Code, http.StatusNoContent)
}
