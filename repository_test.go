package main

import (
	"github.com/magiconair/properties/assert"
	"sync"
	"testing"
)

func Test_Repository_AddTodo(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}

	text := "test"
	todo := Todo{Text: text}

	response := repository.add(todo)

	assert.Equal(t, response.Text, text)

	value, ok := store[response.Id]
	assert.Equal(t, ok, true)
	assert.Equal(t, value.Text, text)
	assert.Equal(t, value.IsMarked, false)
}

func Test_Repository_GetTodosWhenStoreIsEmpty(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}

	response := repository.get()

	assert.Equal(t, len(response), len(store))
}

func Test_Repository_GetTodosWhenStoreIsFull(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	todo := Todo{Id: 1, Text: "test"}
	store[1] = todo

	response := repository.get()

	assert.Equal(t, len(store), len(response))
	_, exists := store[todo.Id]
	assert.Equal(t, exists, true)
}

func Test_Repository_UnMarkWhenTodoIsNotExist(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1

	response := repository.revertMark(id, false)

	assert.Equal(t, response, false)
}

func Test_Repository_UnMarkTodo(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1
	todo := Todo{Id: id, Text: "test", IsMarked: true}
	store[1] = todo

	response := repository.revertMark(id, false)

	assert.Equal(t, response, true)
}

func Test_Repository_MarkWhenTodoIsNotExist(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1

	response := repository.revertMark(id, true)

	assert.Equal(t, response, false)
}

func Test_Repository_MarkTodo(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1
	todo := Todo{Id: id, Text: "test", IsMarked: false}
	store[1] = todo

	response := repository.revertMark(id, true)

	assert.Equal(t, response, true)
}

func Test_Repository_DeleteUnKnownTodo(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1

	isSuccess := repository.delete(id)

	assert.Equal(t, isSuccess, false)
}

func Test_Repository_DeleteKnownTodo(t *testing.T) {
	store := map[int64]Todo{}
	lock := sync.RWMutex{}

	repository := &Repository{
		store,
		lock,
	}
	var id int64 = 1
	todo := Todo{Id: id, Text: "test", IsMarked: false}
	store[1] = todo

	isSuccess := repository.delete(id)

	assert.Equal(t, isSuccess, true)
}