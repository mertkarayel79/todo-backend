package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
)

type Handler struct {
	repository IRepository
}

type IHandler interface {
	todo(w http.ResponseWriter, req *http.Request)
	todos(w http.ResponseWriter, req *http.Request)
}

func NewHandler() IHandler {
	return &Handler{
		repository: NewRepository(),
	}
}

func (i *Handler) todo(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPatch {
		revertMarkedTodo(i.repository, w, req)
	} else if req.Method == http.MethodDelete {
		deleteTodo(i.repository, w, req)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (i *Handler) todos(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		addTodo(i.repository, w, req)
	} else if req.Method == http.MethodGet {
		getTodos(i.repository, w, req)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func getTodos(r IRepository, w http.ResponseWriter, req *http.Request) {
	w.Header().Set(HeaderContentType, MIMEApplicationJSONCharsetUTF8)
	w.WriteHeader(http.StatusOK)

	response := r.get()
	json.NewEncoder(w).Encode(response)
}

func addTodo(r IRepository, w http.ResponseWriter, req *http.Request) {
	var request TodoRequest

	err := json.NewDecoder(req.Body).Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	textTrim := strings.Trim(request.Text, "\t \n")

	if textTrim == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set(HeaderContentType, MIMEApplicationJSONCharsetUTF8)
	w.WriteHeader(http.StatusCreated)

	response := r.add(Todo{Text: request.Text})
	json.NewEncoder(w).Encode(response)
}

func revertMarkedTodo(r IRepository, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	idString := vars["id"]

	id, err := strconv.ParseInt(idString, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var todoMarkRequest TodoMarkRequest

	err = json.NewDecoder(req.Body).Decode(&todoMarkRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	isExist := r.revertMark(id, todoMarkRequest.IsMarked)

	if isExist {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func deleteTodo(r IRepository, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	idString := vars["id"]

	id, err := strconv.ParseInt(idString, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	isSuccess := r.delete(id)

	if isSuccess {
		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}