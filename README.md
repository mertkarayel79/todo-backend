# Todo App Backend
This project contains backend for todo app.

| Project link:| [http://35.244.210.132](http://35.244.210.132) |
| -----------  | ----------- |

## Requirements

- [go](https://golang.org/)

## Features
* Get todo list
* Adding new todo to list
* Removing todo from list
* mark/unmark todo in the list
* service health

## Files and Their Processes

```
.
├── main.go
├── handler.go/handler_test.go
├── model.go                            # Backend interactions are defined under this folder.
├── repository.go/repository_test.go
├── integration_test.go                 # integration tests for backend
├── provider_test.go                    # pact producer tests
├── .gitlab-ci.yml                      # Pipeline file
├── Dockerfile
├── kubernetes.yml                      # Deployment and service definition are in this file.
├── README.md
```
## Build and Tests
### Building the project

Project setup
```
$ go install
```

Compiles
```
$ go build -o main .
```

### Running tests

Run your unit tests
```
$ go test -run . -v
```

Especially run and publish your provider tests
```
$ go test -run TestPactProvider -v
```

## Deploy Project
[Gitlab CI/CD pipeline](.gitlab-ci.yml) is defined with 3 stage. (test, build, deploy)

## Design Notes
* RWMutex was added to solve concurrency problems.

| Tables           | Http Method    | Endpoint  | Response Status |
|------------------|-------------|--------------|-----------------|
| Get todo list    | GET         |  /todos      | 200             |
| Add todo         | POST        |  /todos      | 201, 400        |
| Remove todo      | DELETE      |  /todo/{id}  | 200, 400, 404   |
| Mark/Unmark todo | PATCH       |  /todo/{id}  | 204, 400, 404   |
| Service health   | GET         |  /health     | 200             |

## Bugs
- [ ] Pact is not working as expected in the pipeline.

## License

[GNU GENERAL PUBLIC LICENSE](LICENSE)