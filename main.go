package main

import (
    "encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
)

func main() {
	httpHandler := httpHandlers()
	handler := cors.AllowAll().Handler(httpHandler)
	fmt.Println("Application is running on 8090...")
	http.ListenAndServe(":8090", handler)
}

func httpHandlers() *mux.Router {
	handler := NewHandler()

	r := mux.NewRouter()
	r.HandleFunc("/todos", handler.todos)
	r.HandleFunc("/todo/{id}", handler.todo)
	r.HandleFunc("/health", func(w http.ResponseWriter, request *http.Request) {
		w.Header().Set(HeaderContentType, MIMEApplicationJSONCharsetUTF8)
		w.WriteHeader(http.StatusOK)
		a := map[string]string{"status": "ok"}
		json.NewEncoder(w).Encode(a)
	})

	return r
}